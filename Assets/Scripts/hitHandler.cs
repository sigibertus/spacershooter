﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitHandler : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "bullteTag") {  
            scoreDisplay.score++;   // score wird erhöht wenn ein gegner getötet wird
            Destroy(collision.gameObject);  // Bullet
            Destroy(gameObject);    // gegner wird zerstört             
        }
    }
}

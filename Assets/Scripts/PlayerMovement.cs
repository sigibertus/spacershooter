﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Runtime.CompilerServices;
using UnityEditorInternal;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float playerSpeed = 1f;
    public int life = 10;
    public float noHitTime = 1f;
    public float timeBetweenBlinking = 1f;
    public float speedPowerUpTime = 1f;
    public float speedPowerUpSpeed = 2f;
    public AudioSource damageSound;

    private float startingPlayerSpeed;
    private float speedPowerUpTimeLeft = 0f;
    private float betweenBlink = 0f;
    private float noHit = 0f;

    private PlayerControls controls;
    private Vector2 move;
    private Vector2 rotate;
    private bool bLeftClick = false;
    private Vector2 mousePosition;

    #region Shooting
    public Transform firepoint_left;
    public Transform firepoint_right;
    public GameObject bulletPrefab;
    public float StartCooldown = 0.1f;

    private bool bIsShooting = false;
    private bool shootRight = true;
    private float cooldown;

    private void Shooting(){
        if (cooldown <= 0){ // Cooldown wir runter gezählt
            Shoot();
            cooldown = StartCooldown;   // cooldown wird zurückgesetzt
        } else {
            cooldown -= Time.deltaTime; // cooldown wird runter gezählt
        }
    }

    private void Shoot(){
        if (!bIsShooting) return;

        if (shootRight)
            Instantiate(bulletPrefab, firepoint_right.position, firepoint_right.rotation);  // shoot with right gun
        else 
            Instantiate(bulletPrefab, firepoint_left.position, firepoint_left.rotation);    // shoot with left gun
        
        shootRight = !shootRight;   // change for the next shoot
    }
    #endregion

    #region InputMethods
    private void Awake()
    {
        // Initializing Controls
        controls = new PlayerControls();

        // moving with Controller
        controls.Gameplay.Move.performed += ctx => move = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => move = Vector2.zero;

        // rotate with Controller
        controls.Gameplay.Rotate.performed += ctx => rotate = ctx.ReadValue<Vector2>();
        controls.Gameplay.Rotate.canceled += ctx => rotate = Vector2.zero;

        // Left Click makes it Possible to use mouse
        controls.Gameplay.MouseShoot.performed += ctx => bLeftClick = true;
        controls.Gameplay.MouseShoot.canceled += ctx => bLeftClick = false;

        // setup turn to mouse position
        controls.Gameplay.MousePostition.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
        controls.Gameplay.MousePostition.canceled += ctx => mousePosition = Vector2.zero;

        // setup shoot when a turn method is used
        controls.Gameplay.Rotate.performed += ctx => bIsShooting = true;
        controls.Gameplay.Rotate.canceled += ctx => bIsShooting = false;
        controls.Gameplay.MouseShoot.performed += ctx => bIsShooting = true;
        controls.Gameplay.MouseShoot.canceled += ctx => bIsShooting = false;
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
    #endregion

    private void Start()
    {
        startingPlayerSpeed = playerSpeed;  // anfangspeed wird abgepseichert

    }

    // Update is called once per frame
    void Update()
    {
        Moving();
        Shooting();
        Rotating();
        Blinking();
        ApplySpeedPowerUp();

        lifeDisplay.life = life;    // Wert für Lebensanzeige aktualisieren
        noHit -= Time.deltaTime;    // zeit wo spieler nicht gehittet wird runterzählen
        betweenBlink -= Time.deltaTime; //  zeit wo Raumschiff unsichtbar und sichtbar ist runterzählen
        speedPowerUpTimeLeft -= Time.deltaTime; // Zeit runterzählen, damit das speedPowerUp nur begrenzt aktiv ist

    }

    #region MovementOptions
    private void Moving()
    {
        rb.velocity = move * playerSpeed;
    }

    private void Rotating()
    {
        if (bLeftClick){
            Vector2 diff = Camera.main.ScreenToWorldPoint(mousePosition) - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        } else if (rotate.x != 0 || rotate.y != 0){  // player does not rotate to top when joysticks is untouched
            float angle = Mathf.Atan2(-rotate.x, rotate.y) * Mathf.Rad2Deg;   // z rotation erstellen (wegen 2d muss nur z rotiert werden)
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));    // rotatiob wird angewendet
        }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    { // killing the player
        if (noHit <= 0 && (collision.tag == "enemyTag" || collision.tag == "meteorTag"))
        {   // überprüfen ob der hit damage ist
            if (life <= 0)
            {                    // spieler stirbt bei 0 leben und damage
                damageSound.Play();             // damage sound wird abgespielt
                SceneManager.LoadScene("Menu"); // loading menu
                wrmhlRead.myDevice.close();     // closing serial connection
                Destroy(gameObject);
            }
            else
            {
                life--;                         // leben wird runtergezählt 
                damageSound.Play();             // damagesound wird abgespielt
                noHit = noHitTime;
            }
        }
        if (collision.CompareTag("speedPowerUp"))
        {
            speedPowerUp();
            Destroy(collision.gameObject);      // speedPowerUp aus der Welt löschen
        }
    }

    private void Blinking()
    {
        if (betweenBlink <= 0 && noHit >= 0)
        {
            GetComponent<Renderer>().enabled = !GetComponent<Renderer>().enabled; // changes visible state (blinking effect)
            betweenBlink = timeBetweenBlinking; // Zeit für den unsichtbar/sichtbar mode zurücksetzen
        }
        else if (!(GetComponent<Renderer>().enabled) && noHit < 0)
        { // macht spieler sichtbar wenn er am ende des Blinkens unsichtbar ist
            GetComponent<Renderer>().enabled = true;
        }
    }

    private void ApplySpeedPowerUp()
    {
        if (speedPowerUpTimeLeft > 0)
        {
            playerSpeed = speedPowerUpSpeed;    // wenn das speedPowerUp an ist wird die geschwindigkeit geändert
        }
        else
        {
            playerSpeed = startingPlayerSpeed;  // wenn das speedPowerUp an ist wird die geschwindigkeit zurückgesetzt
        }
    }

    private void speedPowerUp()
    {
        speedPowerUpTimeLeft = speedPowerUpTime; // speedPowerUp wird aktiviert
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 20f;
    public Rigidbody2D rb;

    private float timeInAir = 0;


    // Use this for initialization
    void Start() {
        rb.velocity = transform.up * speed; // Bullet fliegt hierdurch immer in eine richtung
    }

    private void Update() {
        timeInAir += Time.deltaTime;
        if (timeInAir >= 6) {
            Destroy(gameObject);    // löschen der Bullet, um eine zu große anzahl an Bullets zu vermeiden
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("meteorTag")) {    // Bullet löschen wenn meteor  getroffen wird
            Destroy(gameObject);
        }
    }
}

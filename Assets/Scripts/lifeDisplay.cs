﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lifeDisplay : MonoBehaviour {

    public static int life = 0;
    public Text lifeText; 
	
	// Update is called once per frame
	void Update () {
        lifeText.text = "Life: " + life;    // Leben wird mit der derzeitiegen Lebensanzahl überschrieben
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;
    public GameObject meteor;
    public GameObject shootingEnemy;
    public GameObject speedPowerUp;
    public int minDistance = 1;
    public int maxDistance = 5;
    public float startingBasicEnemySpawnRate = 1f;
    public float startingMeteorSpawnrate = 1f;
    public float startingShootingEnemySpawnrate = 1f;
    public float startingSpeedPowerUpSpawnRate = 1f;

    private float basicEnemySpawnrate;
    private float meteorSpawnrate;
    private float shootingEnemySpawnrate;
    private float speedPowerUpSpawnRate;
    private System.Random rnd;

    private void Start() {
        rnd = new System.Random(System.DateTime.Now.Millisecond);
        basicEnemySpawnrate = startingBasicEnemySpawnRate; // iniatilisiere Spawnraten
        meteorSpawnrate = startingMeteorSpawnrate;
        shootingEnemySpawnrate = startingShootingEnemySpawnrate;
        speedPowerUpSpawnRate = startingSpeedPowerUpSpawnRate;
    }

    // Update is called once per frame
    void Update () {    // verschiedene Spawnmethoden werden aufgerufen
        spawningBasicEnemys();
        meteorSpawner();
        shootingEnemySpawner();
        spawnSpeedPowerUp();
    }

    private void spawningBasicEnemys() {
        if (meteorSpawnrate <= 0) {
            // spawnpoint wird berechnet
            Vector3 spawnVector = new Vector3(transform.position.x + rnd.Next(minDistance, maxDistance), transform.position.y + rnd.Next(minDistance, maxDistance)); // bestimmung der Position, mit random wert für entfernung von Spieler
            int getminus = rnd.Next(0, 4); // random für minuswert sorgen, damit auch im Minusbereichs des koordinatensystems gespawnt werden kann
            switch (getminus) {
                case 1:
                    spawnVector = new Vector3(spawnVector.x, -spawnVector.y);
                    break;
                case 2:
                    spawnVector = new Vector3(-spawnVector.x, spawnVector.y);
                    break;
                case 3:
                    spawnVector = -spawnVector;
                    break;
            }

            Instantiate(enemy, spawnVector, new Quaternion(0, 0, 0, 0));    // spawn the objekt
            basicEnemySpawnrate = startingBasicEnemySpawnRate;  // spawnrate zurücksetzen
        } else {
            basicEnemySpawnrate -= Time.deltaTime;    // spawnrate runterzählen
        }
    }

    void meteorSpawner() {
        if (meteorSpawnrate <= 0) {
            // spawnpoint wird berechnet
            Vector3 spawnVector = new Vector3(transform.position.x + rnd.Next(minDistance, maxDistance), transform.position.y + rnd.Next(minDistance, maxDistance));// bestimmung der Position, mit random wert für entfernung von Spieler
            int getminus = rnd.Next(0, 4); // random für minuswert sorgen, damit auch im Minusbereichs des koordinatensystems gespawnt werden kann
            switch (getminus) {
                case 1:
                    spawnVector = new Vector3(spawnVector.x, -spawnVector.y);
                    break;
                case 2:
                    spawnVector = new Vector3(-spawnVector.x, spawnVector.y);
                    break;
                case 3:
                    spawnVector = -spawnVector;
                    break;
            }

            Instantiate(meteor, spawnVector, new Quaternion(0, 0, 0, 0));    // spawn the objekt
            meteorSpawnrate = startingMeteorSpawnrate;  // spawnrate zurücksetzen
        } else {
            meteorSpawnrate -= Time.deltaTime;    // spawnrate runterzählen
        }
    }

    void shootingEnemySpawner() {
        if (shootingEnemySpawnrate <= 0) {
            // spawnpoint wird berechnet
            Vector3 spawnVector = new Vector3(transform.position.x + rnd.Next(minDistance, maxDistance), transform.position.y + rnd.Next(minDistance, maxDistance));// bestimmung der Position, mit random wert für entfernung von Spieler
            int getminus = rnd.Next(0, 4); // random für minuswert sorgen, damit auch im Minusbereichs des koordinatensystems gespawnt werden kann
            switch (getminus) {
                case 1:
                    spawnVector = new Vector3(spawnVector.x, -spawnVector.y);
                    break;
                case 2:
                    spawnVector = new Vector3(-spawnVector.x, spawnVector.y);
                    break;
                case 3:
                    spawnVector = -spawnVector;
                    break;
            }

            Instantiate(shootingEnemy, spawnVector, new Quaternion(0, 0, 0, 0));    // spawn the objekt
            shootingEnemySpawnrate = startingMeteorSpawnrate;  // spawnrate zurücksetzen
        } else {
            shootingEnemySpawnrate -= Time.deltaTime;    // spawnrate runterzählen
        }
    }

    void spawnSpeedPowerUp() {
        if (speedPowerUpSpawnRate <= 0) {
            // spawnpoint wird berechnet
            Vector3 spawnradius = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight)); // cameraFeld berechnen
            Vector3 spawnVector = new Vector3(transform.position.x + rnd.Next((int) -spawnradius.x, (int) spawnradius.x), transform.position.y + rnd.Next((int) -spawnradius.y, (int) spawnradius.y));  // power up spawn in dem cameraFeld
            int getminus = rnd.Next(0, 4); // random für minuswert sorgen, damit auch im Minusbereichs des koordinatensystems gespawnt werden kann
            switch (getminus) {
                case 1:
                    spawnVector = new Vector3(spawnVector.x, -spawnVector.y);
                    break;
                case 2:
                    spawnVector = new Vector3(-spawnVector.x, spawnVector.y);
                    break;
                case 3:
                    spawnVector = -spawnVector;
                    break;
            }

            Instantiate(speedPowerUp, spawnVector, new Quaternion(0, 0, 0, 0));    // spawn the objekt
            speedPowerUpSpawnRate = startingSpeedPowerUpSpawnRate;  // spawnrate zurücksetzen
        } else {
            speedPowerUpSpawnRate -= Time.deltaTime;    // spawnrate runterzählen
        }
    }
}

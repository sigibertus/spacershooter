﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour {

    public float movementSpeed = 1f;

    private Transform playerTransform;

    private void Start() {
        playerTransform = GameObject.Find("playerShip1_blue").transform;    // beim spawnen des Gegner wird der Spieler, gesucht und der Transform gespeichert
    }

    // Update is called once per frame
    void Update () {
        Rotating();
        Moving();
	}

    private void Rotating() {   // Rotierung von Gegner in Richtung des Spielers
        float angle = Mathf.Atan2(playerTransform.position.y - transform.position.y, playerTransform.position.x - transform.position.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + 90));
    }

    private void Moving() { // gegner geht in Richtung des Spielers
        transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, movementSpeed * Time.deltaTime);
    }

        
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highScore : MonoBehaviour {

    public Text highScoreText;

    private void Update() {
        if (scoreDisplay.score >= PlayerPrefs.GetInt("highScore", 0)) {
            PlayerPrefs.SetInt("highScore", scoreDisplay.score);    // highScore überschreiben wenn er geknackt wurde
        }

        highScoreText.text = "Highscore: " + PlayerPrefs.GetInt("highScore", 0);    // text für den highscore verändern
    }
}

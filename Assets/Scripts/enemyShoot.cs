﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyShoot : MonoBehaviour {

    public Transform firepoint_left;
    public Transform firepoint_right;
    public GameObject bulletPrefab;
    public float StartCooldown = 0.1f;

    private bool shootRight = true;
    private float cooldown;

    private void Start() {
        cooldown = StartCooldown;    // cooldown initialisieren
    }

    // Update is called once per frame
    void Update() {
        if (cooldown <= 0) { // Cooldown wir runter gezählt
            Shoot();    // methode zum schießen aufrufen
            cooldown = StartCooldown;
        } else {
            cooldown -= Time.deltaTime; // cooldown wird runter gezählt
        }

    }

    private void Shoot() {
        if (shootRight) {
            Instantiate(bulletPrefab, firepoint_right.position, firepoint_right.rotation  * new Quaternion(0,0,180,0));  // shoot with right gun
        } else {
            Instantiate(bulletPrefab, firepoint_left.position, firepoint_left.rotation * new Quaternion(0, 0, 180, 0));    // shoot with left gun
        }
        shootRight = !shootRight;   // change for the next shoot
    }
}

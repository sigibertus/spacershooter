﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sceneLoader : MonoBehaviour {

    public Text comEingabe;

    public void goToGame() {
        wrmhlRead.comPort = comEingabe.text;    // eingabe für ComPort wird übergene
        scoreDisplay.score = 0;                 // score wird auf 0 gesetzt, da sonst der letzte score behalten wird
        SceneManager.LoadScene("game");         // das Spiel wird geladen
    }
}

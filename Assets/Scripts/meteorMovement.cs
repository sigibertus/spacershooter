﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteorMovement : MonoBehaviour {

    public float speed = 1f;
    public int middleDistance = 1;

    private Rigidbody2D rb;
    private System.Random rnd;

	// Use this for initialization
	void Start () {
        rnd = new System.Random(System.DateTime.Now.Millisecond);  
        rb = GetComponent<Rigidbody2D>(); // getting the rigidbody of the gameObject
        Rotate();
        rb.velocity = transform.up * speed; // eine flugrichtung wird bestimmt, Richtung ist die zuvor bestimmte Rotation
    }

    void Rotate() { // rotiert ungefähr in die mitte, hierbei ist ein random wert dabei, damit nicht jeder meteor genau in die Mitte fliegt
        float angle = Mathf.Atan2(Camera.main.transform.position.y + rnd.Next(-middleDistance, middleDistance), Camera.main.transform.position.x + rnd.Next(-middleDistance, middleDistance)) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreDisplay : MonoBehaviour {

    public static int score = 0;
    public Text scoreText;

    private void Update() {
        scoreText.text = "Score: " + score; // score wird mit dem derzeitiegen Score überschrieben
    }
}

<h1>Space Shooter</h1>

A Space Shooter in Unity using Arduino Controller

The Controllers on Arduino

![Joysticks on The Arduino](/readmeImages/joysticksMicrocontroller.jpeg "Controller")

This is what the game looks like in the Unity Editor

![Joysticks on The Arduino](/readmeImages/unityScreenshot.PNG "Controller")
